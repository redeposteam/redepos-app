package br.com.redepos.revendedor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.yanzhenjie.zbar.Image;
import com.yanzhenjie.zbar.ImageScanner;
import com.yanzhenjie.zbar.Symbol;
import com.yanzhenjie.zbar.SymbolSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.redepos.AbstractActivity;
import br.com.redepos.CameraPreview;
import br.com.redepos.R;
import br.com.redepos.lib.Alert;
import br.com.redepos.lib.Graphic;
import br.com.redepos.lib.Preferences;
import br.com.redepos.lib.Util;
import br.com.redepos.revendedor.tasks.EnvioQrcodeTask;

public class EnvioQrcodeActivity extends AbstractActivity {

    private static final int REQUEST_SCANNER_QRCODE = 1;
    private static final int REQUEST_IMAGE_QRCODE = 2;

    private SharedPreferences preferences;

    private String qrcode;
    private ImageView iescanear;
    private ImageView iescolher;
    private TextView lescolher;
    private Button enviarDigitos;
    private Button limparDigitos;

    private TextView numeroLinha;
    private EditText DV;
    private EditText N1;
    private EditText N2;
    private EditText N3;
    private EditText N4;
    private EditText N5;

    protected static int linhaTotal = 0;
    protected static int linhaAtual = 1;
    private ArrayList<String> linhas = new ArrayList<>();

    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    private ListView qrcodeLinhasListView;

    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envio_qrcode);
        this.definirEstilos();

        // PREFERENCIAS

        preferences = Preferences.getSharedPreferences(this);

        // RECUPERANDO OBJETOS

        iescanear = (ImageView) findViewById(R.id.envioqrcode_imagem_escanear);
        iescolher = (ImageView) findViewById(R.id.envioqrcode_imagem_selecionar);
        lescolher = (TextView) findViewById(R.id.envioqrcode_label_informe2);
        enviarDigitos = (Button) findViewById(R.id.envioqrcode_botao_enviardigitos);
        limparDigitos = (Button) findViewById(R.id.envioqrcode_botao_limpardigitos);

        numeroLinha = (TextView) findViewById(R.id.envioqrcode_label_numerolinha);

        DV = (EditText) findViewById(R.id.envioqrcode_campo_dv);
        N1 = (EditText) findViewById(R.id.envioqrcode_campo_n1);
        N2 = (EditText) findViewById(R.id.envioqrcode_campo_n2);
        N3 = (EditText) findViewById(R.id.envioqrcode_campo_n3);
        N4 = (EditText) findViewById(R.id.envioqrcode_campo_n4);
        N5 = (EditText) findViewById(R.id.envioqrcode_campo_n5);

        numeroLinha.setText(getResources().getString(R.string.envioqrcode_label_numerolinha)+ ": " + linhaAtual);

        int fonteDigitos = 20;

        DV.setTextSize(fonteDigitos);
        N1.setTextSize(fonteDigitos);
        N2.setTextSize(fonteDigitos);
        N3.setTextSize(fonteDigitos);
        N4.setTextSize(fonteDigitos);
        N5.setTextSize(fonteDigitos);

        String[] items = {};
        qrcodeLinhasListView = (ListView) findViewById(R.id.qrcodelinhas_listview);
        arrayList = new ArrayList<>(Arrays.asList(items));
        adapter = new ArrayAdapter<String>(this, R.layout.list_item_text, R.id.linha, arrayList);
        qrcodeLinhasListView.setAdapter(adapter);

        // EVENTOS DE INTERFACE

        iescanear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scanner();
            }
        });

        // SELECIONAR ARQUIVO

        if(Integer.parseInt(preferences.getString("usuarioPerfil", "0")) == 4) {
            iescolher.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    selecionar();
                }
            });
        } else {
            iescolher.setVisibility(View.GONE);
            lescolher.setVisibility(View.GONE);
        }

        // MAPEANDO OS EVENTOS DE DIGITAO DOS CODIGOS DE QRCODE

        campoKeyUp(null, DV,   N1, 2);
        campoKeyUp(	 DV, N1,   N2, 5);
        campoKeyUp(	 N1, N2,   N3, 5);
        campoKeyUp(	 N2, N3,   N4, 5);
        campoKeyUp(	 N3, N4,   N5, 5);

        N5.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( event.getAction() == KeyEvent.ACTION_UP ) {
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if(N5.length() == 0) {
                            N4.requestFocus();
                        }
                        return true;
                    } else {
                        if(N5.length() == 5) {
                            if(validarLinha()) {

                                if(linhas.size() == 0) {
                                    linhaTotal = (int) Math.ceil((Integer.parseInt(N1.getText().subSequence(2, 5).toString() + N2.getText().subSequence(0, 1).toString())+18)/25.0);
                                }

                                if(linhaAtual == 1) {
                                    linhas.add(N2.getText().subSequence(3, 5).toString()+N3.getText().toString()+N4.getText().toString()+N5.getText().toString());
                                } else {
                                    linhas.add(N1.getText().toString()+N2.getText().toString()+N3.getText().toString()+N4.getText().toString()+N5.getText().toString());
                                }

                                arrayList.add(String.format("%03d", linhaAtual).toString()+": "+DV.getText().toString()+"-"+N1.getText().toString()+"."+N2.getText().toString()+"."+N3.getText().toString()+"."+N4.getText().toString()+"."+N5.getText().toString());
                                adapter.notifyDataSetChanged();
                                linhaAtual++;

                                numeroLinha.setText(getResources().getString(R.string.envioqrcode_label_numerolinha)+ ": " + linhaAtual);

                                DV.setText("");
                                N1.setText("");
                                N2.setText("");
                                N3.setText("");
                                N4.setText("");
                                N5.setText("");

                                if(linhaAtual > linhaTotal) {
                                    DV.setEnabled(false);
                                    N1.setEnabled(false);
                                    N2.setEnabled(false);
                                    N3.setEnabled(false);
                                    N4.setEnabled(false);
                                    N5.setEnabled(false);
                                    numeroLinha.setText(getResources().getString(R.string.envioqrcode_message_numerolinha));
                                } else {
                                    DV.requestFocus();
                                }
                            } else {
                                mensagem("QRCode Digitvel", "Linha Invlida");
                            }
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        limparDigitos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                limparDigitos();
            }
        });

        enviarDigitos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                enviarDigitos();
            }
        });
    }

    protected void mensagem(String titulo, String mensagem) {

        Alert.show(this, titulo, mensagem);
    }

    protected void enviarDigitos() {

        qrcode = TextUtils.join("", linhas);
        enviar();
        limparDigitos();
    }

    protected void limparDigitos() {

        arrayList.clear();
        adapter.notifyDataSetChanged();

        if(linhas.size() > 0) {
            linhas.clear();
        }

        linhaAtual = 1;
        linhaTotal = 0;
        numeroLinha.setText(getResources().getString(R.string.envioqrcode_label_numerolinha)+ ": " + linhaAtual);

        DV.setEnabled(true);
        N1.setEnabled(true);
        N2.setEnabled(true);
        N3.setEnabled(true);
        N4.setEnabled(true);
        N5.setEnabled(true);

        DV.setText("");
        N1.setText("");
        N2.setText("");
        N3.setText("");
        N4.setText("");
        N5.setText("");

        DV.requestFocus();
    }

    protected boolean validarLinha() {

        boolean valido = true;
        String dv1 = DV.getText().toString();

        if(linhaAtual == 1) {

            String dv2 = Util.dac_11(N1.getText().toString() + N2.getText().subSequence(0,1),2,9);

            //Validar Primerio DAC

            if(!N2.getText().subSequence(1,3).toString().equals(dv2)) {
                valido = false;
            }

            String sequencia = "";
            sequencia += N1.getText().toString();
            sequencia += N2.getText().toString();
            sequencia += N3.getText().toString();
            sequencia += N4.getText().toString();
            sequencia += N5.getText().toString();
            sequencia += String.format("%02d", linhaAtual);

            //Validar DAC Linha

            if(!Util.dac_11(sequencia,2,9).equals(dv1)) {
                valido = false;
            }

        } else {

            String sequencia = "";
            sequencia += N1.getText().toString();
            sequencia += N2.getText().toString();
            sequencia += N3.getText().toString();
            sequencia += N4.getText().toString();
            sequencia += N5.getText().toString();
            sequencia += String.format("%02d", linhaAtual);

            //Validar DAC Linha

            if(!Util.dac_11(sequencia,2,9).equals(dv1)) {
                valido = false;
            }
        }

        return valido;
    }

    protected void campoKeyUp(final EditText anterior, final EditText atual, final EditText proximo, final int digitos) {
        atual.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( event.getAction() == KeyEvent.ACTION_UP) {
                    if(keyCode == KeyEvent.KEYCODE_DEL) {
                        if(atual.length() == 0 && anterior != null) {
                            anterior.requestFocus();
                        }
                        return true;
                    } else {
                        if(atual.length() == digitos && proximo != null) {
                            proximo.requestFocus();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void enviar() {

        String idTipoLoteria = "";

        try {
            JSONArray produtos = new JSONArray(preferences.getString("produtos", ""));
            JSONObject json = new JSONObject(produtos.get(0).toString());
            idTipoLoteria = json.get("ID_TIPO_LOTERIA").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new EnvioQrcodeTask(EnvioQrcodeActivity.this).execute
                (
                        preferences.getString("usuarioPerfil",""),
                        preferences.getString("usuarioLogin", ""),
                        preferences.getString("usuarioSenha", ""),
                        preferences.getString("hash", ""),
                        idTipoLoteria,
                        qrcode,
                        idTipoLoteria
                );
    }

    public void scanner() {

        Intent intentScanner = new Intent(this, ZbarActivity.class);
        intentScanner.putExtra("labelCaptura", getResources().getString(R.string.envioqrcode_label_captura));
        intentScanner.putExtra("cor", CameraPreview.BORDER_BLUE);
        startActivityForResult(intentScanner, 1);
    }

    public void selecionar() {

        Intent intentImage = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intentImage, 2);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //ENCERRAMENTO

        if(REQUEST_SCANNER_QRCODE == requestCode && RESULT_OK == resultCode) {

            qrcode = data.getStringExtra("SCAN_RESULT");
            enviar();
        }

        if(REQUEST_IMAGE_QRCODE == requestCode && RESULT_OK == resultCode) {

            // CAPTURANDO CAMINHO DO ARQUIVO e CRIANDO IMAGEM PARA MANIPULACAO

            String picturePath = Util.pathFile(this, data.getData(), new String[]{MediaStore.Images.Media.DATA});
            Bitmap barcodeGallery = BitmapFactory.decodeFile(picturePath);

            // REDIMENSIONANDO

            int[] dimensions = Graphic.calcResize(barcodeGallery, 300);
            barcodeGallery = Bitmap.createScaledBitmap(barcodeGallery, dimensions[0], dimensions[1], true);
            barcodeGallery = Graphic.toGrayscale(barcodeGallery, Bitmap.Config.RGB_565);

            // CRIANDO IMAGEM PARA ESCANEAMENTO

            Image barcode = new Image(dimensions[0], dimensions[1], "RGB4");
            int[] pixels = new int[barcode.getWidth() * barcode.getHeight()];
            barcodeGallery.getPixels(pixels, 0, barcode.getWidth(), 0, 0, barcode.getWidth(), barcode.getHeight());
            barcode.setData(pixels);

            // ESCANEANDO IMAGEM

            ImageScanner scanner = new ImageScanner();
            int result = scanner.scanImage(barcode.convert("Y800"));
            if(result > 0) {
                qrcode = "";
                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
                    qrcode+= sym.getData();
                }
                enviar();
            } else {
                Toast.makeText(this, getResources().getString(R.string.erro_qrcode_image), Toast.LENGTH_LONG).show();
            }
        }
    }
}
