package br.com.redepos.revendedor.tasks;

import java.util.HashMap;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.app.ProgressDialog;
import android.content.ContentValues;

import br.com.redepos.BuildConfig;
import br.com.redepos.R;
import br.com.redepos.db.DB;
import br.com.redepos.lib.Util;
import br.com.redepos.lib.Http;
import br.com.redepos.lib.Alert;
import br.com.redepos.revendedor.MainActivity;
import br.com.redepos.lib.Constants;
import br.com.redepos.lib.Preferences;

public class LoginTask extends AsyncTask<String, Void, String> {

    Context context;
    JSONObject json;
    ProgressDialog progress;

    public LoginTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {

        progress = new ProgressDialog(this.context);
        progress.setMessage(this.context.getString(R.string.login_message_efetuando_login));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected String doInBackground(String... params) {

        String retorno = null;

        // LOGIN ON-LINE

        if(Util.isInternetConnected(context)) {

            // CONFIGURACAO DO SERVICO

            Http http = new Http();
            http.addParameter("tipoTransacao", Constants.TRANSACAO_LOGIN);
            http.addParameter("login", BuildConfig.SERVICE_LOGIN);
            http.addParameter("senha", BuildConfig.SERVICE_SENHA);

            // VARIAVEIS DA TRANSACAO

            http.addParameter("usuarioPerfil",params[0]);
            http.addParameter("usuarioLogin", params[1]);
            http.addParameter("usuarioSenha", params[2]);
            //http.addParameter("hash", params[3]);
            http.addParameter("imei", Util.getImei(context));
            http.addParameter("versaoApp", Util.getAppVersion(context));

            String transacao;
            try {
                transacao = http.send(BuildConfig.SERVICE);
            } catch (MalformedURLException e) {
                transacao = "{\"codigo_execucao\":\""+Constants.CONNECTION_ERRO_URL+"\"}";
            } catch (TimeoutException e) {
                transacao = "{\"codigo_execucao\":\""+Constants.CONNECTION_TIMEOUT+"\"}";
            } catch (IOException e) {
                transacao = "{\"codigo_execucao\":\""+Constants.CONNECTION_ERRO+"\"}";
            }

            // JSON

            try {

                json = new JSONObject(transacao);

                if (json.getString("codigo_execucao").equals("0") && !json.getJSONObject("dados").getString("nome_usuario").equals("null")) {
                    retorno = "0";
                } else {
                    retorno = json.getString("codigo_execucao");
                }

                // SALVANDO PREFERENCIAS

                if (retorno.equals("0")) {

                    HashMap<String, String> valores = new HashMap<>();
                    valores.put("usuarioPerfil",params[0]);
                    valores.put("usuarioLogin", params[1]);
                    valores.put("usuarioSenha", params[2]);
                    //valores.put("hash", params[3]);
                    valores.put("produtos", json.getJSONObject("dados").getString("produtos"));
                    valores.put("versao_autorizada", json.getJSONObject("dados").getString("versao_autorizada"));
                    Preferences.setStrings((Activity) context, valores);

                    // SALVANDO BANCO DE DADOS ENVIADAS

                    DB db = new DB(context);

                    ContentValues dados = new ContentValues();
                    dados.put("perfil",valores.get("usuarioPerfil"));
                    dados.put("login", valores.get("usuarioLogin"));
                    dados.put("senha", valores.get("usuarioSenha"));
                    //dados.put("hash", valores.get("hash"));
                    dados.put("dados", transacao);

                    Cursor cursor = db.selecionar("usuario", new String[]{"*"}, "perfil = ? AND login = ?", new String[]{valores.get("usuarioPerfil"), valores.get("usuarioLogin")}, null, null, null);

                    if(cursor.moveToFirst()) {
                        db.atualizar("usuario", "id", dados);
                    } else {
                        db.inserir("usuario", dados);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            // LOGIN OFF-LINE

        } else {

            DB db = new DB(context);
            String usuarioPerfil = params[0];
            String usuarioLogin  = params[1];
            String usuarioSenha  = params[2];

            Cursor cursor = db.selecionar("usuario", new String[]{"*"}, "perfil = ? AND login = ?", new String[]{usuarioPerfil, usuarioLogin}, null, null, null);

            if(cursor.moveToFirst()) {
                if(cursor.getString(cursor.getColumnIndex("login")).equals(usuarioLogin)
                        && cursor.getString(cursor.getColumnIndex("senha")).equals(usuarioSenha)) {

                    HashMap<String, String> valores = new HashMap<>();
                    valores.put("usuarioPerfil",cursor.getString(cursor.getColumnIndex("perfil")));
                    valores.put("usuarioLogin", cursor.getString(cursor.getColumnIndex("login")));
                    valores.put("usuarioSenha", cursor.getString(cursor.getColumnIndex("senha")));
                    //valores.put("hash", cursor.getString(cursor.getColumnIndex("hash")));

                    try {
                        json = new JSONObject(cursor.getString(cursor.getColumnIndex("dados")));
                        valores.put("produtos", json.getJSONObject("dados").getString("produtos"));
                        valores.put("versao_autorizada", json.getJSONObject("dados").getString("versao_autorizada"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Preferences.setStrings((Activity) context, valores);

                    retorno = "0";
                } else {
                    retorno = Constants.CONNECTION_ERRO;
                }
            } else {
                retorno = Constants.CONNECTION_ERRO;
            }
        }
        return retorno;
    }

    protected void onPostExecute(String param) {

        final String retorno = param;

        if (!retorno.equals("0")) {

            progress.dismiss();

            if(retorno.equals(Constants.CONNECTION_ERRO_URL)) {

                Alert.show((Activity) context, this.context.getString(R.string.dialog_title_connection), this.context.getString(R.string.erro_connection_url));

            } else if(retorno.equals(Constants.CONNECTION_ERRO)) {

                Alert.show((Activity) context, this.context.getString(R.string.dialog_title_connection), this.context.getString(R.string.erro_connection));

            } else if(retorno.equals(Constants.CONNECTION_TIMEOUT)) {

                Alert.show((Activity) context, this.context.getString(R.string.dialog_title_connection), this.context.getString(R.string.erro_connection_timeout));

            } else {

                Alert.show((Activity) context, this.context.getString(R.string.dialog_title_login), this.context.getString(R.string.login_message_erro_login));
            }

        } else {

            progress.setMessage(this.context.getString(R.string.login_message_sucesso_login));
            new Thread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            }).start();
        }
    }
}
