package br.com.redepos.revendedor.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import br.com.redepos.AuditoriaActivity;
import br.com.redepos.R;
import br.com.redepos.revendedor.EnvioQrcodeActivity;

public class Menu {

    class Acoes {

        private Context context;
        private Class<?> classe;

        public Acoes(Context context, Class<?> classe) {

            this.context = context;
            this.classe = classe;
        }

        public void executar() {

            Intent intent = new Intent(context, classe);
            getActivity().startActivityForResult(intent, 0);
        }
    }

    private static Context MenuContext;

    protected Activity getActivity() {

        return ((Activity) Menu.MenuContext);
    }

    public Menu(Context MenuContext) {

        Menu.MenuContext = MenuContext;

        final Acoes[] acoes = new Acoes[] {
                new Acoes(Menu.MenuContext, EnvioQrcodeActivity.class)
                ,new Acoes(Menu.MenuContext, AuditoriaActivity.class)
        };

        final int[] opcoes = new int[] {
                R.drawable.ic_qrcode
                ,R.drawable.ic_historico
        };

        final String[] opcoestexto = new String[] {
                "ENVIO DE QRCODE"
                ,"HISTÓRICO"
        };

        GridView gridview = (GridView) getActivity().findViewById(R.id.menuGridView);
        gridview.setAdapter(new MenuAdaptador(MenuContext, opcoes, opcoestexto));

        gridview.setColumnWidth(100);

        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)  {
                acoes[position].executar();
            }
        });
    }
}

