package br.com.redepos.revendedor.menu;

import android.view.View;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;

import br.com.redepos.R;

@SuppressLint("InflateParams")
public class MenuAdaptador extends BaseAdapter {

    private int[] opcoes;
    private String[] opcoestexto;
    private Context context;

    public MenuAdaptador(Context context, int[] opcoes, String[] opcoestexto) {

        this.opcoes = opcoes;
        this.opcoestexto = opcoestexto;
        this.context = context;
    }

    @Override
    public int getCount() {

        return opcoes.length;
    }

    @Override
    public Object getItem(int position) {

        return opcoes[position];
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public static class ViewHolder {

        public ImageView imgViewFlag;
        public TextView txtViewTitle;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder view;
        LayoutInflater inflator = ((Activity) context).getLayoutInflater();

        if(convertView==null) {

            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.gridview_menu, null);
            //convertView = inflator.inflate(R.layout.gridview_menu_grande, null);

            view.txtViewTitle = (TextView) convertView.findViewById(R.id.textView1);
            view.imgViewFlag = (ImageView) convertView.findViewById(R.id.imageView1);

            convertView.setTag(view);

        } else {

            view = (ViewHolder) convertView.getTag();
        }

        view.txtViewTitle.setText(opcoestexto[position]);
        view.imgViewFlag.setImageResource(opcoes[position]);

        return convertView;
    }
}
