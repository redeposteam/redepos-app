package br.com.redepos.revendedor.tasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import br.com.redepos.BuildConfig;
import br.com.redepos.R;
import br.com.redepos.db.DB;
import br.com.redepos.lib.Alert;
import br.com.redepos.lib.Constants;
import br.com.redepos.lib.Http;
import br.com.redepos.lib.Preferences;
import br.com.redepos.lib.Util;
import br.com.redepos.revendedor.EnvioQrcodeActivity;

public class EnvioQrcodeTask extends AsyncTask<String, Void, String> {

    private JSONObject json;
    private Context context;
    private ProgressDialog progress;
    private static MediaPlayer MpICQ;

    public EnvioQrcodeTask(Context context) {

        this.context = context;
        progress = new ProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {

        progress.setMessage(this.context.getString(R.string.envioqrcode_message_enviando_qrcode));
        progress.show();
    }

    @Override
    protected String doInBackground(String... params) {

        String retorno = null;

        HashMap<String, String> valorpreferencia = new HashMap<>();
        valorpreferencia.put("opcaoLoteriaSelecionada",params[5]);
        Preferences.setStrings((Activity) context, valorpreferencia);

        // ENVIO ON-LINE

        if(Util.isInternetConnected(context)) {

            // CONFIGURACAO DO SERVICO

            Http http = new Http();
            http.addParameter("tipoTransacao", Constants.TRANSACAO_ENCERRAMENTO);
            http.addParameter("login", BuildConfig.SERVICE_LOGIN);
            http.addParameter("senha", BuildConfig.SERVICE_SENHA);

            // VARIAVEIS DA TRANSACAO

            http.addParameter("usuarioPerfil",params[0]);
            http.addParameter("usuarioLogin", params[1]);
            http.addParameter("usuarioSenha", params[2]);
            http.addParameter("hash", params[3]);
            http.addParameter("idTipoLoteria",params[4]);
            http.addParameter("dados", params[5]);
            http.addParameter("imei", Util.getImei(context));
            http.addParameter("versaoApp", Util.getAppVersion(context));

            try {
                retorno = http.send(BuildConfig.SERVICE);
            } catch (MalformedURLException e) {
                retorno = "{\"codigo_execucao\":\""+ Constants.CONNECTION_ERRO_URL+"\"}";
            } catch (TimeoutException e) {
                retorno = "{\"codigo_execucao\":\""+Constants.CONNECTION_TIMEOUT+"\"}";
            } catch (IOException e) {
                retorno = "{\"codigo_execucao\":\""+Constants.CONNECTION_ERRO+"\"}";
            }
            if(retorno == null) {
                retorno = "{\"codigo_execucao\":\"-999\",\"mensagem\":\"\"}";
            }

            // JSON

            try {

                json = new JSONObject(retorno);
                int status = Constants.STATUS_ERRO;
                if(!json.getString("codigo_execucao").equals("-999")) {
                    status = Constants.STATUS_OK;
                }

                // SALVANDO BANCO DE DADOS ENVIADAS
                DB db = new DB(context);
                ContentValues valores = new ContentValues();
                valores.put("id_tipo_loteria", params[4]);
                valores.put("loteria", params[4]);
                valores.put("codigo",  params[5]);
                if(status == Constants.STATUS_OK) {
                    valores.put("enviado", Constants.ENVIOQRCODE_ENVIADO);
                } else {
                    valores.put("enviado", Constants.ENVIOQRCODE_ENVIADO_ERRO);
                }
                valores.put("perfil",params[0]);
                valores.put("login", params[1]);
                valores.put("senha", params[2]);
                valores.put("data_criacao", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
                valores.put("data_envio", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
                valores.put("transacao", retorno);
                long id = db.inserir("encerramento", valores);
                this.inserirAuditoria(retorno, json, status, id, params);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            // ENVIO OFF-LINE

        } else {

            retorno = "{\"codigo_execucao\":\"-000\",\"mensagem\":\"\"}";

            // SALVANDO BANCO DE DADOS ENVIADAS

            DB db = new DB(context);
            ContentValues valores = new ContentValues();
            valores.put("id_tipo_loteria", params[4]);
            valores.put("loteria", params[4]);
            valores.put("codigo",  params[5]);
            valores.put("enviado", Constants.ENVIOQRCODE_PENDENTE);
            valores.put("perfil", params[0]);
            valores.put("login", params[1]);
            valores.put("senha", params[2]);
            valores.put("data_criacao", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
            valores.put("data_envio", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
            long id = db.inserir("encerramento", valores);
            this.inserirAuditoria(retorno, json, 0, id, params);
        }

        return retorno;
    }

    private void inserirAuditoria(String retorno, JSONObject json, int status, long id, String... params) {
        try {
            DB db = new DB(context);
            ContentValues valores = new ContentValues();
            valores.put("id_tipo_loteria", params[3]);
            valores.put("loteria", params[5]);
            valores.put("codigo", params[4]);
            valores.put("status", status);
            valores.put("evento", Constants.EVENTO_QRCODE);
            valores.put("usuario", params[1]);
            valores.put("data", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
            if (json != null) {
                JSONObject dados = json.getJSONObject("dados");
                if (dados.getString("DATA_HORA_SERVIDOR") != null) {
                    String dataServidor = dados.getString("DATA_HORA_SERVIDOR");
                    valores.put("data_servidor", dataServidor);
                }
            }
            valores.put("id_encerramento", id);
            valores.put("transacao", retorno);
            db.inserir("auditoria", valores);
        } catch (Exception ex) {
            Log.e("", ex.getMessage());
        }
    }

    protected void onPostExecute(String param) {

        progress.dismiss();

        // JSON

        String codigo = null;
        String mensagem = null;

        try {
            json = new JSONObject(param);
            codigo = json.getString("codigo_execucao");
            mensagem = json.getString("mensagem");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // DIALOG PARA NOVO ENVIO

        if(MpICQ == null){
            MpICQ = MediaPlayer.create(this.context, R.raw.icq);
        }

        AlertDialog.Builder builder = new
                AlertDialog.Builder((Activity) context);

        if(codigo.equals(Constants.CONNECTION_ERRO_URL)) {

            Alert.show((Activity) context, this.context.getString(R.string.dialog_title_connection), this.context.getString(R.string.erro_connection_url));

        } else if(codigo.equals(Constants.CONNECTION_ERRO)) {

            Alert.show((Activity) context, this.context.getString(R.string.dialog_title_connection), this.context.getString(R.string.erro_connection));

        } else if(codigo.equals(Constants.CONNECTION_TIMEOUT)) {

            builder.setTitle(this.context.getString(R.string.dialog_title_connection)).setMessage(context.getString(R.string.erro_connection_timeout)+":\n"+ "Deseja tentar novamente ?");

        } else {

            if(!codigo.equals("-999")) {

                if(codigo.equals("-000")) {

                    builder.setTitle(this.context.getString(R.string.dialog_title_encerramento)).setMessage(context.getString(R.string.envioqrcode_message_sucesso_off_line) +":\n"+ "Deseja enviar outro QRCode ?");

                } else {

                    builder.setTitle(this.context.getString(R.string.dialog_title_encerramento)).setMessage(context.getString(R.string.envioqrcode_message_sucesso_envio) +":\n"+ "Deseja enviar outro QRCode ?");
                }

            } else {

                MpICQ.start();
                builder.setTitle(this.context.getString(R.string.dialog_title_encerramento)).setMessage(context.getString(R.string.envioqrcode_message_erro_envio) +":\n"+ mensagem +"\n\n"+ "Deseja tentar novamente ?");
            }

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() { public void onClick(DialogInterface dialog, int id) {

                Intent intent = new Intent(context, EnvioQrcodeActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }});

            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() { public void onClick(DialogInterface dialog, int id) {

                Intent intent = new Intent(context, EnvioQrcodeActivity.class);
                intent.putExtra("enviar_novamente", true);
                context.startActivity(intent);
                ((Activity) context).finish();
            }});

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
