package br.com.redepos.revendedor;

//import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.DialogInterface;
import android.content.SharedPreferences;
//import android.database.Cursor;

//import br.com.redepos.db.DB;
import br.com.redepos.BuildConfig;
import br.com.redepos.R;
import br.com.redepos.lib.Util;
import br.com.redepos.lib.Preferences;
import br.com.redepos.revendedor.tasks.LoginTask;

public class LoginActivity extends Activity {

    private SharedPreferences preferences;
    private EditText usuario;
    private EditText senha;
    //	private EditText hash;
//	private TextView lHash;
    private Button enviar;
    private Button limpar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.setVersionProperties();

        // PREFERENCIAS

        preferences = Preferences.getSharedPreferences(this);

        // CONFIGURACOES

        String sUsuarioLogin = preferences.getString("usuarioLogin", "");

        // RECUPERANDO OBJETOS

        usuario = (EditText) findViewById(R.id.login_campo_usuario);
        senha = (EditText) findViewById(R.id.login_campo_senha);
        enviar = (Button) findViewById(R.id.login_botao_enviar);
        limpar = (Button) findViewById(R.id.login_botao_limpar);
        //hash = (EditText) findViewById(R.id.login_campo_hash);
        //lHash = (TextView) findViewById(R.id.login_label_hash);

        // DADOS DE INTERFACE

        usuario.setText(sUsuarioLogin);
        //hash.setText("1kw0ePIBF8");
        //hash.setEnabled(false);

        // EVENTOS DE INTERFACE
		/*
		if(preferences.getString("hash", "").length() > 0) {

			hash.setVisibility(View.INVISIBLE);
			lHash.setVisibility(View.INVISIBLE);
			hash.setText(preferences.getString("hash", ""));
		}
		*/
        usuario.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    senha.requestFocus();
                    return true;
                }
                return false;
            }
        });
		/*
		usuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {

				DB db = new DB(LoginActivity.this);
				Cursor cursor = db.selecionar("usuario", new String[]{"*"}, "login = ?", new String[]{usuario.getText().toString()}, null, null, null);

				boolean ocultarCampoHash = true;

				HashMap<String, String> parametros = new HashMap<>();

				if(cursor.moveToFirst()) {
					if(cursor.getString(cursor.getColumnIndex("hash")).isEmpty()) {
						ocultarCampoHash = false;
					}
				} else {
					ocultarCampoHash = false;
				}

				if(ocultarCampoHash) {

					hash.setVisibility(View.INVISIBLE);
					lHash.setVisibility(View.INVISIBLE);
					hash.setText(cursor.getString(cursor.getColumnIndex("hash")));
					parametros.put("hash",hash.getText().toString());
					Preferences.setStrings(LoginActivity.this, parametros);

				} else {

					hash.setText("");
					hash.setVisibility(View.VISIBLE);
					lHash.setVisibility(View.VISIBLE);
					parametros.put("hash","");
					Preferences.setStrings(LoginActivity.this, parametros);
				}

				cursor.close();
			}
		});
		*/
        senha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    enviar.callOnClick();
                    return true;
                }
                return false;
            }
        });

        limpar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                senha.setText(null);
                usuario.setText(null);
            }
        });

        enviar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                new LoginTask(LoginActivity.this).execute
                        (
                                "3",
                                usuario.getText().toString().trim(),
                                senha.getText().toString().trim()
                                //hash.getText().toString().trim()
                        );
            }
        });
    }

    private void setVersionProperties() {
        ((TextView) findViewById(R.id.versao)).setText(Util.getAppVersionName(this));
        TextView buildType = findViewById(R.id.buildType);
        TextView flavor = findViewById(R.id.flavor);
        if (BuildConfig.DEBUG_MODE) {
            buildType.setVisibility(View.VISIBLE);
            flavor.setVisibility(View.VISIBLE);
            buildType.setText(String.format("Build: %s", BuildConfig.BUILD_TYPE));
            flavor.setText(String.format("Flavor: %s", BuildConfig.FLAVOR));
        } else {
            buildType.setVisibility(View.GONE);
            flavor.setVisibility(View.GONE);
        }
    }

    protected void onResume() {

        super.onResume();

        if(!Util.isServiceRunning(this, "br.com.redepos.services.UpdateService")) {

            if(usuario.getText().toString().length() > 0) {
                senha.requestFocus();
            }

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getApplicationContext().getString(R.string.dialog_title_update_app)).setMessage(getApplicationContext().getString(R.string.app_message_download));
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        }
    }
}
