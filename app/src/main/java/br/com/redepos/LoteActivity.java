package br.com.redepos;

import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.content.Context;
import android.widget.ImageView;

import android.content.SharedPreferences;

import br.com.redepos.lib.Util;
import br.com.redepos.lib.Preferences;
import br.com.redepos.tasks.LoteSolicitarTask;
import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;

public class LoteActivity extends AbstractActivity {

	private EditText codigo1, codigo2, codigo3; 
	
	private SharedPreferences preferences;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lote);
		this.definirEstilos();
		
		// PREFERENCIAS
		
		preferences = Preferences.getSharedPreferences(this);
		
		// RECUPERANDO OBJETOS

		codigo1 = (EditText) findViewById(R.id.lote_campo_codigo1);
		codigo2 = (EditText) findViewById(R.id.lote_campo_codigo2);
		codigo3 = (EditText) findViewById(R.id.lote_campo_codigo3);
		
		MaskEditTextChangedListener maskCodigo1 = new MaskEditTextChangedListener("####.###.###-###", codigo1);
		MaskEditTextChangedListener maskCodigo2 = new MaskEditTextChangedListener("####.###.###-###", codigo2);		
		MaskEditTextChangedListener maskCodigo3 = new MaskEditTextChangedListener("####.###.###-###", codigo3);		
		
		codigo1.addTextChangedListener(maskCodigo1);
		codigo2.addTextChangedListener(maskCodigo2);
		codigo3.addTextChangedListener(maskCodigo3);
		
		Button solicitar = (Button) findViewById(R.id.lote_botao_solicitarlote);

		// EVENTOS DE INTERFACE
		
		solicitar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				solicitar();
			}
		});
		
		campoKeyUp(   null, codigo1, codigo2, 16, getApplicationContext());
		campoKeyUp(codigo1, codigo2, codigo3, 16, getApplicationContext());
		campoKeyUp(codigo2, codigo3,    null, 16, getApplicationContext());
		
		codigo1.requestFocus();
	}

	protected void campoKeyUp(final EditText anterior, final EditText atual, final EditText proximo, final int digitos, final Context context) {
		atual.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					if (keyCode == KeyEvent.KEYCODE_DEL) {
						if (atual.length() == 0 && anterior != null) {
							anterior.requestFocus();
						}
						return true;
					} else {
						if (atual.length() == digitos) {
							String codigo = atual.getText().toString().replace(".", "").replace("-", "");
							String[] partes = getResources().getResourceEntryName(atual.getId()).split("_");
							ImageView  imagem = (ImageView) findViewById(getResources().getIdentifier(partes[2] + "_status", "id", context.getPackageName()));
							if (validarCodigo(codigo)) {
								imagem.setImageResource(R.drawable.ic_stat_ok);
								if (proximo != null) {
									proximo.requestFocus();
								}
							} else {
								imagem.setImageResource(R.drawable.ic_stat_erro);
							}
						}
						return true;
					}
				}
				return false;
			}
		});
	}
	
	protected boolean validarCodigo(String codigo) {

		String numero = (String) codigo.subSequence(0, codigo.length()-1);
		String digito = (String) codigo.subSequence(codigo.length()-1, codigo.length());
		return Util.dac_10(numero, 1).equals(digito);
	}

	public void solicitar() {

		new LoteSolicitarTask(LoteActivity.this).execute
		(
			preferences.getString("usuarioPerfil",""),
			preferences.getString("usuarioLogin", ""),
			preferences.getString("usuarioSenha", ""),
			codigo1.getText().toString().replace(".", "").replace("-", ""),
			codigo2.getText().toString().replace(".", "").replace("-", ""),
			codigo3.getText().toString().replace(".", "").replace("-", ""),
			codigo1.getText().toString(),
			codigo2.getText().toString(),
			codigo3.getText().toString()
		);
	}
}
