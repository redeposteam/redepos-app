package br.com.redepos.services;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.JobIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import br.com.redepos.BuildConfig;
import br.com.redepos.R;
import br.com.redepos.db.DB;
import br.com.redepos.lib.Constants;
import br.com.redepos.lib.Http;
import br.com.redepos.lib.Notifify;
import br.com.redepos.lib.Util;

public class NotificacaoService extends JobIntentService {

	private Timer mTimer = null;
	public static final int JOB_ID = 0x01;

	public static void enqueueWork(Context context, Intent work) {
		enqueueWork(context, NotificacaoService.class, JOB_ID, work);
	}

	@Override
	protected void onHandleWork(@NonNull Intent intent) {

		if(mTimer != null) {
			mTimer.cancel();
		} else {
			mTimer = new Timer();
		}

		//mTimer.scheduleAtFixedRate(new EnviosPendentesTask(), 0, (10 * 60) * 1000);
		mTimer.scheduleAtFixedRate(new EnviosPendentesTask(), 0, 5000);
	}

//	public void onCreate() {
//
//		if(mTimer != null) {
//			mTimer.cancel();
//		} else {
//			mTimer = new Timer();
//		}
//	}

	@Override
	public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

//	public int onStartCommand(Intent intent, int flags, int startId) {
//		mTimer.scheduleAtFixedRate(new EnviosPendentesTask(), 0, (10 * 60) * 1000);
//		return START_STICKY;
//	}

	class EnviosPendentesTask extends TimerTask {

		public void run() {

			// BANCO DE DADOS - ENCERRAMENTO

			Util.getImei(getApplicationContext());
			DB db = new DB(getApplicationContext());
			Cursor cursor = db.selecionar("encerramento", new String[]{"*"}, "enviado = ?", new String[]{"0"}, null, null, null);

			if(cursor.getCount() > 0) {

				if(Util.isInternetConnected(getApplicationContext())) {

					// CONFIGURACAO DO SERVICO

					Http http = new Http();
					//http.addParameter("tipoTransacao", Constants.TRANSACAO_ENCERRAMENTO);
					if (BuildConfig.FLAVOR.equals("vendedor")) {
						http.addParameter("tipoTransacao", Constants.TRANSACAO_QRCODE);
					} else {
						http.addParameter("tipoTransacao", Constants.TRANSACAO_ENCERRAMENTO);
					}
					http.addParameter("login", BuildConfig.SERVICE_LOGIN);
					http.addParameter("senha", BuildConfig.SERVICE_SENHA);

					int enviados = 0;
					int total = cursor.getCount();

					while(cursor.moveToNext()) {

						http.removeParameterArray(new String[]{"usuarioPerfil","usuarioLogin","usuarioSenha","idTipoLoteria","dados"});

						http.addParameter("usuarioPerfil",cursor.getString(cursor.getColumnIndex("perfil")));
						http.addParameter("usuarioLogin", cursor.getString(cursor.getColumnIndex("login")));
						http.addParameter("usuarioSenha", cursor.getString(cursor.getColumnIndex("senha")));
						http.addParameter("idTipoLoteria",cursor.getInt(cursor.getColumnIndex("id_tipo_loteria"))+"");
						http.addParameter("dados", cursor.getString(cursor.getColumnIndex("codigo")));
						http.addParameter("imei", Util.getImei(getApplicationContext()));
						http.addParameter("versaoApp", Util.getAppVersion(getApplicationContext()));


						try {

							String transacao = http.send(BuildConfig.SERVICE);

							JSONObject json = new JSONObject(transacao);
							int status = Constants.STATUS_ERRO;
							if(!json.getString("codigo_execucao").equals("-999")) {
								status = Constants.STATUS_OK;
							}

							ContentValues dados = new ContentValues();
							dados.put("id", cursor.getInt(cursor.getColumnIndex("id")));
							dados.put("enviado", 1);
							dados.put("data_envio", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
							dados.put("transacao", transacao);
							db.atualizar("encerramento", "id", dados);

							// SALVANDO BANCO DE DADOS AUDITORIA

							dados = new ContentValues();
							dados.put("status", status);
							dados.put("transacao", transacao);
							if (status != Constants.STATUS_ERRO) {
								JSONObject dadosJson = json.getJSONObject("dados");
								if (dadosJson.getString("DATA_HORA_SERVIDOR") != null) {
									String dataServidor = dadosJson.getString("DATA_HORA_SERVIDOR");
									dados.put("data_servidor", dataServidor);
								}
							}
							long id = cursor.getInt(cursor.getColumnIndex("id"));
							db.atualizarStatusAuditoria(id, "id_encerramento", dados);
//							dados = new ContentValues();
//							dados.put("id_tipo_loteria", cursor.getInt(cursor.getColumnIndex("id_tipo_loteria")));
//							dados.put("loteria", cursor.getString(cursor.getColumnIndex("loteria")));
//							dados.put("codigo", cursor.getString(cursor.getColumnIndex("codigo")));
//							dados.put("status", status);
//							dados.put("evento", Constants.EVENTO_ENCERRAMENTO);
//							dados.put("usuario", cursor.getString(cursor.getColumnIndex("login")));
//							dados.put("data", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
//							JSONObject jsonDados = json.getJSONObject("dados");
//							if (jsonDados.getString("DATA_HORA_SERVIDOR") != null) {
//								String dataServidor = jsonDados.getString("DATA_HORA_SERVIDOR");
//								dados.put("data_servidor", dataServidor);
//							}
//							dados.put("transacao", transacao);
//							db.inserir("auditoria", dados);

							enviados++;

						} catch (JSONException e) {

						} catch (MalformedURLException e) {

						} catch (TimeoutException e) {

						} catch (IOException e) {

						}
					}
					cursor.close();

					// NOTIFICACAO

					String tituloRapido = "";
					String titulo = "Encerramentos";
					String mensagem = "";

					if(enviados > 1) {
						tituloRapido = "Encerramentos Enviados";
						mensagem = enviados+" encerramentos de "+total+" enviados com sucesso";
					} else {
						tituloRapido = "Encerramento Enviado";
						mensagem = "1 encerramento enviado com sucesso";
					}
					Notifify.show(getApplicationContext(), null, tituloRapido, titulo, mensagem, R.drawable.ic_redepos_nt, R.drawable.ic_redepos, new long[]{300, 150, 300, 150, 600}, null);

				} else {

					// NOTIFICACAO

					String tituloRapido = "";
					String titulo = "Encerramentos";
					String mensagem = "";

					if(cursor.getCount() > 1) {
						tituloRapido = "Encerramentos Pendentes";
						mensagem = "Existem "+cursor.getCount()+" encerramentos pendentes";
					} else {
						tituloRapido = "Encerramento Pendente";
						mensagem = "Existe 1 encerramento pendente";
					}
					Notifify.show(getApplicationContext(), null, tituloRapido, titulo, mensagem, R.drawable.ic_redepos_nt, R.drawable.ic_redepos, new long[]{300, 150, 300, 150, 600}, null);
				}
			}

			// BANCO DE DADOS - ARRECADACAO

			cursor = db.selecionar("arrecadacao", new String[]{"*"}, "enviado = ?", new String[]{"0"}, null, null, null);

			if(cursor.getCount() > 0) {

				if(Util.isInternetConnected(getApplicationContext())) {

					// CONFIGURACAO DO SERVICO

					Http http = new Http();
					http.addParameter("tipoTransacao", Constants.TRANSACAO_ARRECADACAO);
					http.addParameter("login", BuildConfig.SERVICE_LOGIN);
					http.addParameter("senha", BuildConfig.SERVICE_SENHA);

					int enviados = 0;
					int total = cursor.getCount();

					while(cursor.moveToNext()) {

						http.removeParameterArray(new String[]{"usuarioPerfil","usuarioLogin","usuarioSenha","idTipoLoteria","dados"});

						http.addParameter("usuarioPerfil",cursor.getString(cursor.getColumnIndex("perfil")));
						http.addParameter("usuarioLogin", cursor.getString(cursor.getColumnIndex("login")));
						http.addParameter("usuarioSenha", cursor.getString(cursor.getColumnIndex("senha")));
						http.addParameter("idTipoLoteria",cursor.getInt(cursor.getColumnIndex("id_tipo_loteria"))+"");
						http.addParameter("dados", cursor.getString(cursor.getColumnIndex("codigo")));
						http.addParameter("imei", Util.getImei(getApplicationContext()));
						http.addParameter("versaoApp", Util.getAppVersionName(getApplicationContext()));

						try {

							String transacao = http.send(BuildConfig.SERVICE);

							JSONObject json = new JSONObject(transacao);
							int status = Constants.STATUS_ERRO;
							if(!json.getString("codigo_execucao").equals("-999")) {
								status = Constants.STATUS_OK;
							}

							ContentValues dados = new ContentValues();
							dados.put("id", cursor.getInt(cursor.getColumnIndex("id")));
							dados.put("enviado", 1);
							dados.put("data_envio", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
							dados.put("transacao", transacao);
							db.atualizar("arrecadacao", "id", dados);

							// SALVANDO BANCO DE DADOS AUDITORIA

							dados = new ContentValues();
							dados.put("status", status);
							dados.put("transacao", transacao);
							if (json != null && json.getString("DATA_HORA_SERVIDOR") != null) {
								String dataServidor = json.getString("DATA_HORA_SERVIDOR");
								dados.put("data_servidor", dataServidor);
							}

							long id = cursor.getInt(cursor.getColumnIndex("id"));
							db.atualizarStatusAuditoria(id, "id_arrecadacao", dados);

//							dados = new ContentValues();
//							dados.put("id_tipo_loteria", cursor.getInt(cursor.getColumnIndex("id_tipo_loteria")));
//							dados.put("loteria", cursor.getString(cursor.getColumnIndex("loteria")));
//							dados.put("codigo", cursor.getString(cursor.getColumnIndex("codigo")));
//							dados.put("status", status);
//							dados.put("evento", Constants.EVENTO_ARRECADACAO);
//							dados.put("usuario", cursor.getString(cursor.getColumnIndex("login")));
//							dados.put("data", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
//							JSONObject jsonDados = json.getJSONObject("dados");
//							if (jsonDados.getString("DATA_HORA_SERVIDOR") != null) {
//								String dataServidor = jsonDados.getString("DATA_HORA_SERVIDOR");
//								dados.put("data_servidor", dataServidor);
//							}
//							dados.put("transacao", transacao);
//							db.inserir("auditoria", dados);

							enviados++;

						} catch (JSONException e) {

						} catch (MalformedURLException e) {

						} catch (TimeoutException e) {

						} catch (IOException e) {

						}
					}
					cursor.close();

					// NOTIFICACAO

					String tituloRapido = "";
					String titulo = "Encerramentos";
					String mensagem = "";

					if(enviados > 1) {
						tituloRapido = "Arrecadaes Enviadas";
						mensagem = enviados+" arrecadaes de "+total+" enviadas com sucesso";
					} else {
						tituloRapido = "Arrecadao Enviada";
						mensagem = "1 arrecadao enviada com sucesso";
					}
					Notifify.show(getApplicationContext(), null, tituloRapido, titulo, mensagem, R.drawable.ic_redepos_nt, R.drawable.ic_redepos, new long[]{300, 150, 300, 150, 600}, null);

				} else {

					// NOTIFICACAO

					String tituloRapido = "";
					String titulo = "Arrecadao";
					String mensagem = "";

					if(cursor.getCount() > 1) {
						tituloRapido = "Arrecadaes Pendentes";
						mensagem = "Existem "+cursor.getCount()+" arrecadaes pendentes";
					} else {
						tituloRapido = "Arrecadao Pendente";
						mensagem = "Existe 1 arrecadao pendente";
					}
					Notifify.show(getApplicationContext(), null, tituloRapido, titulo, mensagem, R.drawable.ic_redepos_nt, R.drawable.ic_redepos, new long[]{300, 150, 300, 150, 600}, null);
				}
			}
		}
	}
}
