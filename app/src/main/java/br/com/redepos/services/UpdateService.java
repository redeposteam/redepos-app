package br.com.redepos.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;
import org.json.JSONException;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;

import br.com.redepos.BuildConfig;
import br.com.redepos.R;
import br.com.redepos.lib.Http;
import br.com.redepos.lib.Util;
import br.com.redepos.AvisoActivity;
import br.com.redepos.lib.Constants;

public class UpdateService extends JobIntentService {

	private Worker worker;
	public static final int JOB_ID = 0x01;

	public static void enqueueWork(Context context, Intent work) {
		enqueueWork(context, UpdateService.class, JOB_ID, work);
	}

	@Override
	protected void onHandleWork(@NonNull Intent intent) {
		this.worker = new Worker(JOB_ID);
		this.worker.start();
	}

//	@Override
//	public int onStartCommand(Intent intent, int flags, int startId) {
//
//		this.worker = new Worker(startId);
//		this.worker.start();
//		return super.onStartCommand(intent, flags, startId);
//	}

	public void limparNotificacoes() {

		NotificationManager nmngr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		nmngr.cancelAll();
	}

	public void aviso(Exception e) {

		Intent intent = new Intent(this, AvisoActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Context context = this;

		if(e.getClass().getName().equals("java.net.ConnectException")) {
			intent.putExtra("mensagem", context.getString(R.string.erro_connection));
		} else if(e.getClass().getName().equals("java.net.SocketTimeoutException")) {
			intent.putExtra("mensagem", context.getString(R.string.erro_connection_timeout));
		} else {
			intent.putExtra("mensagem", context.getString(R.string.erro));
		}

		startActivity(intent);
	}

	class Worker extends Thread {

		int startId;

		public Worker(int startId) {
			this.startId = startId;
		}

		public void run() {

			String[] file = fileDownload();

			try {

				download(file[0], file[1]);
				installApp();

			}catch(Exception e){

				limparNotificacoes();
				aviso(e);

			} finally {

				stopSelf(this.startId);
			}
		}
	}

	protected String[] fileDownload() {

		JSONObject json;
		String erro = "0";
		String apkFile = null;
		String apkFileUrl = null;
		String novaVersao = null;

		// CONFIGURACAO DO SERVICO

		Http http = new Http();
		http.addParameter("tipoTransacao", Constants.TRANSACAO_ATUALIZACAO);
		http.addParameter("login", BuildConfig.SERVICE_LOGIN);
		http.addParameter("senha", BuildConfig.SERVICE_SENHA);

		// VARIAVEIS DA TRANSACAO

		http.addParameter("versao", Util.getAppVersion(this));

		try {

			novaVersao = http.send(BuildConfig.SERVICE);
			json = new JSONObject(novaVersao);
			apkFile = Constants.DOWNLOAD_APK_FILE;
			apkFileUrl = json.getString("host")+"/"+json.getString("apk");

		} catch (MalformedURLException e) {
			erro = Constants.CONNECTION_ERRO_URL;
		} catch (TimeoutException e) {
			erro = Constants.CONNECTION_TIMEOUT;
		} catch (JSONException e) {
			erro = Constants.CONNECTION_TIMEOUT;
		} catch (IOException e) {
			erro = Constants.CONNECTION_ERRO;
		}

		if(erro.equals("0")) {
			return new String[]{apkFileUrl, apkFile};
		} else {
			return new String[]{};
		}
	}

	private void installApp() {

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory().getPath().toString()+"/"+Constants.DOWNLOAD_APK_FILE)), "application/vnd.android.package-archive");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivity(intent);
	}

	protected void download(String fileUrl, String file) throws Exception {

		MediaPlayer mp = MediaPlayer.create(this, R.raw.iphone);
		NotificationManager nmngr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder = new Notification.Builder(this);
		builder.setPriority(Notification.PRIORITY_DEFAULT);

		builder.setTicker("Atualizao");
		builder.setContentTitle("Atualizao do RedePOS");
		builder.setContentText("Download em Progresso");
		builder.setSmallIcon(R.drawable.ic_redepos_nt);
		builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_redepos));

		nmngr.notify(1, builder.build());
		mp.start();

		URL url = new URL(fileUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(3000);
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);

		if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

			connection.connect();

			File document = new File(Environment.getExternalStorageDirectory().getPath().toString());
			document.mkdirs();

			File outputFile = new File(document, file);
			if (outputFile.exists()) {
				outputFile.delete();
			}

			int lenghtOfFile = connection.getContentLength();
			FileOutputStream fos = new FileOutputStream(outputFile);
			InputStream is = connection.getInputStream();

			int len1 = 0;
			long fim = 0;
			long total = 0;
			long inicio = 0;
			float kbps = 0.0f;
			long tamAmostra = 0;
			int tempoAmostra = 500;
			byte[] buffer = new byte[204800];
			inicio = System.currentTimeMillis();

			int iconSequence = 0;

			mp.start();

			while ((len1 = is.read(buffer)) != -1) {

				fim = System.currentTimeMillis();
				tamAmostra += len1;
				total += len1;

				if (fim - inicio > tempoAmostra) {
					kbps = (125.0f * (float) tamAmostra) / (128.0f * (float) (fim - inicio));
					tamAmostra = 0;
				}

				if (fim - inicio > tempoAmostra) {

					switch (iconSequence) {
						case 0:
							builder.setSmallIcon(R.drawable.ic_redepos_nt_0);
							break;
						case 1:
							builder.setSmallIcon(R.drawable.ic_redepos_nt_1);
							break;
						case 2:
							builder.setSmallIcon(R.drawable.ic_redepos_nt_2);
							break;
						case 3:
							builder.setSmallIcon(R.drawable.ic_redepos_nt_3);
							break;
						case 4:
							builder.setSmallIcon(R.drawable.ic_redepos_nt_4);
							break;
					}

					if(iconSequence == 4) {
						iconSequence = 0;
					} else {
						iconSequence++;
					}
				}

				builder.setContentText(String.format("Download em Progresso %.2f kbps", kbps));
				builder.setProgress(100, (int) ((total * 100) / lenghtOfFile), false);
				nmngr.notify(1, builder.build());

				fos.write(buffer, 0, len1);
				fos.flush();

				if (fim - inicio > tempoAmostra) {
					inicio = System.currentTimeMillis();
				}

				try {
					Thread.sleep(25);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			mp.start();
			fos.close();
			is.close();

			nmngr.cancelAll();
			connection.disconnect();

		} else {

			throw new java.net.ConnectException();
		}
	}
}
