package br.com.redepos;

import android.os.Bundle;
import android.view.Menu;
import android.app.Activity;
import android.view.MenuItem;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;

public abstract class AbstractActivity extends Activity {

	protected void definirEstilos() {

		ActionBar ab = getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
		super.onCreate(savedInstanceState);
	}

	public boolean onCreateOptionsMenu(Menu menu) {

		return super.onCreateOptionsMenu(menu);
	}

	public boolean onMenuItemSelected(int panel, MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home:
				if (!this.getClass().equals(MainActivity.class)) {
					finish();
				} else {
					this.sair();
				}
				break;
			case R.id.atualizar:
				// this.atualizar();
				break;
		}
		return false;
	}

	protected void atualizar() {

	}

	public void finish() {

		super.finish();
		overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
	}

	protected void sair() {

		// DIALOG PARA SAIDA DO APLICATIVO

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getApplicationContext().getString(R.string.app_name)).setMessage(getApplicationContext().getString(R.string.app_message_saida));
		builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				finish();
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
}
