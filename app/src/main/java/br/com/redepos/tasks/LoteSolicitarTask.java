package br.com.redepos.tasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeoutException;

import br.com.redepos.BuildConfig;
import br.com.redepos.LoteActivity;
import br.com.redepos.R;
import br.com.redepos.db.DB;
import br.com.redepos.lib.Alert;
import br.com.redepos.lib.Constants;
import br.com.redepos.lib.Http;
import br.com.redepos.lib.Util;

public class LoteSolicitarTask extends AsyncTask<String, Void, String> {

	private JSONObject json;
	private Context context;
	private ProgressDialog progress;

	public LoteSolicitarTask(Context context) {

		this.context = context;
		progress = new ProgressDialog(context);
	}

	@Override
	protected void onPreExecute() {

		progress.setMessage(this.context
				.getString(R.string.encerramento_message_enviando_encerramento));
		progress.show();
	}

	@Override
	protected String doInBackground(String... params) {

		String retorno = null;

		if (Util.isInternetConnected(context)) {

			// CONFIGURACAO DO SERVICO

			Http http = new Http();
			http.addParameter("tipoTransacao", Constants.TRANSACAO_SOLICITACAO);
			http.addParameter("login", BuildConfig.SERVICE_LOGIN);
			http.addParameter("senha", BuildConfig.SERVICE_SENHA);

			// VARIAVEIS DA TRANSACAO

			http.addParameter("usuarioPerfil", params[0]);
			http.addParameter("usuarioLogin", params[1]);
			http.addParameter("usuarioSenha", params[2]);
			http.addParameter("linha1", params[3]);
			http.addParameter("linha2", params[4]);
			http.addParameter("linha3", params[5]);
			http.addParameter("imei", Util.getImei(context));
			http.addParameter("versaoApp", Util.getAppVersion(context));

			try {
				retorno = http.send(BuildConfig.SERVICE);
			} catch (MalformedURLException e) {
				retorno = "{\"situacao\":\""
						+ Constants.CONNECTION_ERRO_URL + "\"}";
			} catch (TimeoutException e) {
				retorno = "{\"situacao\":\""
						+ Constants.CONNECTION_TIMEOUT + "\"}";
			} catch (IOException e) {
				retorno = "{\"situacao\":\"" + Constants.CONNECTION_ERRO + "\"}";
			}
			if (retorno == null) {
				retorno = "{\"situacao\":\"-999\",\"mensagem\":\"\"}";
			}

			// JSON

			try {

				json = new JSONObject(retorno);
				int status = Constants.STATUS_ERRO;
				if (json.getString("situacao").equals("true")) {
					status = Constants.STATUS_OK;
				}

				// SALVANDO BANCO DE DADOS AUDITORIA

				DB db = new DB(context);
				ContentValues valores = new ContentValues();
				valores.put("id_tipo_loteria", params[3]);
				valores.put("loteria", params[5]);
				valores.put("codigo", params[4]);
				valores.put("status", status);
				valores.put("evento", Constants.EVENTO_SOLICITACAO_LOTE);
				valores.put("usuario", params[1]);
				valores.put("data", Util.dataFormatar(Util.dataHoraAtual(), "yyyy-MM-dd HH:mm:ss"));
				retorno = retorno.substring(0, retorno.length()-1) + ",\"linha1\":\""+params[6]+"\",\"linha2\":\""+params[7]+"\",\"linha3\":\""+params[8]+"\"}";
				JSONObject dados = json.getJSONObject("dados");
				if (dados.getString("DATA_HORA_SERVIDOR") != null) {
					String dataServidor = dados.getString("DATA_HORA_SERVIDOR");
					valores.put("data_servidor", dataServidor);
				}
				valores.put("transacao", retorno);
				db.inserir("auditoria", valores);

			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {

			retorno = "{\"situacao\":\""+Constants.CONNECTION_ERRO+"\"}";
		}

		return retorno;
	}

	protected void onPostExecute(String param) {

		progress.dismiss();

		// JSON

		String codigo = null;
		String mensagem = null;

		try {
			json = new JSONObject(param);
			codigo = json.getString("situacao");
			mensagem = json.getString("mensagem");
			((TextView) ((Activity) context).findViewById(R.id.lote_label_retorno1)).setText(json.getString("retorno1"));
			((TextView) ((Activity) context).findViewById(R.id.lote_label_retorno2)).setText(json.getString("retorno2"));
			((TextView) ((Activity) context).findViewById(R.id.lote_label_retorno3)).setText(json.getString("retorno3"));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// DIALOG PARA NOVO ENVIO

		if (codigo.equals(Constants.CONNECTION_ERRO_URL)) {

			Alert.show((Activity) context,this.context.getString(R.string.dialog_title_connection),this.context.getString(R.string.erro_connection_url));

		} else if (codigo.equals(Constants.CONNECTION_TIMEOUT)) {

			Alert.show((Activity) context,this.context.getString(R.string.dialog_title_connection), context.getString(R.string.erro_connection_timeout));

		} else {

			if (codigo.equals("false")) {

				AlertDialog.Builder builder = new AlertDialog.Builder((Activity) context);

				builder.setTitle(this.context.getString(R.string.dialog_title_lote)).setMessage(context.getString(R.string.encerramento_message_erro_envio)+ ":\n"+ mensagem + "\n\n"+ "Deseja tentar novamente ?");

				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						((Activity) context).finish();
					}
				});

				builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						Intent intent = new Intent(context,LoteActivity.class);
						context.startActivity(intent);
						((Activity) context).finish();
					}
				});

				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}
	}
}
