package br.com.redepos.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.redepos.R;
import br.com.redepos.lib.Constants;
import br.com.redepos.lib.Util;

@SuppressLint("InflateParams")
public class AuditoriaAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<HashMap<String, String>> listItems;

    static class ViewHolder  {
    	
        public ImageView icone;
        public TextView data;
        public TextView dataServidor;
        public TextView usuario;
        public TextView evento;
        public TextView informacao1;
        public TextView informacao2;
        public TextView informacao3;
    }

    public AuditoriaAdapter(Activity context, ArrayList<HashMap<String, String>> listItems) {
    	
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
    	
        View listItemView = convertView;
        
        if(convertView == null) {
        	
            LayoutInflater inflater = context.getLayoutInflater();
            listItemView = inflater.inflate(br.com.redepos.R.layout.list_item_auditoria, null);

            ViewHolder viewHolder = new ViewHolder();
            
	        viewHolder.icone = (ImageView) listItemView.findViewById(R.id.auditoria_status);
	        viewHolder.data = (TextView) listItemView.findViewById(R.id.auditoria_data);
            viewHolder.dataServidor = (TextView) listItemView.findViewById(R.id.auditoria_data_servidor);
	        viewHolder.usuario = (TextView) listItemView.findViewById(R.id.auditoria_usuario);
	        viewHolder.evento = (TextView) listItemView.findViewById(R.id.auditoria_evento);
	        viewHolder.informacao1 = (TextView) listItemView.findViewById(R.id.auditoria_informacao_linha1);
	        viewHolder.informacao2 = (TextView) listItemView.findViewById(R.id.auditoria_informacao_linha2);
	        viewHolder.informacao3 = (TextView) listItemView.findViewById(R.id.auditoria_informacao_linha3);
            listItemView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) listItemView.getTag();
        @SuppressWarnings("unchecked")
		HashMap<String, String> item = (HashMap<String, String>) getItem(position);
        
        if(Integer.parseInt(item.get("status")) == Constants.STATUS_OK) {
        	holder.icone.setImageResource(R.drawable.ic_stat_ok);
        } else {
        	holder.icone.setImageResource(R.drawable.ic_stat_erro);
        }
        
        holder.data.setText(String.format("Data: %s", item.get("data")));
        holder.dataServidor.setText(String.format("Data Servidor: %s", item.get("data_servidor")));
        holder.usuario.setText(item.get("usuario"));
        holder.evento.setText(item.get("evento"));
        holder.informacao1.setVisibility(View.VISIBLE);
        holder.informacao2.setVisibility(View.VISIBLE);
        holder.informacao3.setVisibility(View.VISIBLE);

        switch (item.get("evento")) {
        
			case Constants.EVENTO_ENCERRAMENTO:
	
				try {
					JSONObject json = new JSONObject(item.get("transacao"));
					JSONObject json2 = json.getJSONObject("dados");
					holder.informacao1.setText("Us/Tr: "+ json2.getString("USUARIO") +"   Term: "+ json2.getString("TERMINAL") +"   Ed: "+ json2.getString("EDICAO").substring(0,10));
					holder.informacao2.setText("Controle: "+ json2.getString("CONTROLE") +"  Venda.QRCode: "+ json2.getString("VENDAS_QRCODE") +"   Venda.Total: "+ json2.getString("VENDAS"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
	
			case Constants.EVENTO_SOLICITACAO_LOTE:
	
				try {
					JSONObject json = new JSONObject(item.get("transacao"));
					holder.informacao1.setText("E:"+ json.getString("linha1")  +";"+ json.getString("linha2")  +";"+ json.getString("linha3"));
					holder.informacao2.setText("R:"+ json.getString("retorno1")+";"+ json.getString("retorno2")+";"+ json.getString("retorno3"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
				
			case Constants.EVENTO_ARRECADACAO:
				
				try {
					JSONObject json = new JSONObject(item.get("transacao"));
					JSONObject json2 = json.getJSONObject("dados");
					holder.informacao1.setText("Tr: "+ json2.getString("CODIGO_TERMINAL") +" Data/Hora: "+ Util.dataFormatar(json2.getString("DATA_HORA_PRESTACAO"), "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss"));
					holder.informacao2.setText("Ve: "+ json2.getString("VENDEDOR") +"  Ar: "+ json2.getString("ARRECADADOR"));
					holder.informacao3.setText("Valor Pago: R$ " + Util.moedaFormatar(json2.getString("VALOR_PAGO")) +" Saldo Final: R$ "+ Util.moedaFormatar(json2.getString("SALDO_FINAL")));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
            default:
                holder.informacao1.setVisibility(View.GONE);
                holder.informacao2.setVisibility(View.GONE);
                holder.informacao3.setVisibility(View.GONE);
                break;
		}

        return listItemView;
    }
}