package br.com.redepos;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.redepos.db.DB;
import br.com.redepos.lib.Preferences;
import br.com.redepos.lib.Util;
import br.com.redepos.ui.AuditoriaAdapter;

public class AuditoriaActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auditoria);
		definirEstilos();
		
		SharedPreferences preferences = Preferences.getSharedPreferences(this);
		
		DB db = new DB(this);
		Cursor cursor = db.selecionar("auditoria", new String[]{"*"}, "usuario = ?", new String[]{preferences.getString("usuarioLogin", "")}, null, "data DESC", null);

		ArrayList<HashMap<String, String>> listItems = new ArrayList<HashMap<String, String>>();
		
		if (cursor.moveToFirst()) {
		    do {
		    	HashMap<String, String> item = new HashMap<String, String>();
		    	item.put("status", cursor.getString(cursor.getColumnIndex("status")));
		    	item.put("data",   Util.dataFormatar(cursor.getString(cursor.getColumnIndex("data")), "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss"));
		    	String dataServidor = "---";
				if (cursor.getColumnIndex("data_servidor") != -1 && cursor.getString(cursor.getColumnIndex("data_servidor")) != null) {
					dataServidor = Util.dataFormatar(cursor.getString(cursor.getColumnIndex("data_servidor")), "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss");
				}
				item.put("data_servidor", dataServidor);
		    	item.put("usuario",cursor.getString(cursor.getColumnIndex("usuario")));
		    	item.put("evento", cursor.getString(cursor.getColumnIndex("evento")));
		    	item.put("transacao", cursor.getString(cursor.getColumnIndex("transacao")));
		    	listItems.add(item);
		    } while (cursor.moveToNext());
		}		
		
		ListView list = (ListView) findViewById(R.id.auditoria_listview_tabela);
		AuditoriaAdapter tabela = new AuditoriaAdapter(this, listItems);
		list.setAdapter(tabela);
	}
}
