package br.com.redepos.lib;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("SimpleDateFormat")
public class Util {

	protected static DecimalFormatSymbols dfs;
	protected static NumberFormat formatterNumber;
	
//	public static float getAPIVerison() {
//
//        StringBuilder strBuild = new StringBuilder();
//        strBuild.append(android.os.Build.VERSION.RELEASE.substring(0, 2));
//        return Float.valueOf(strBuild.toString());
//	}
	
	public static PackageInfo getAppVersionInfo(Context context) {
		
		PackageInfo retorno = null;
		try {
			retorno = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
	public static String getAppVersion(Context context) {

		PackageInfo info = Util.getAppVersionInfo(context);
		return info.versionName;
		//return info.versionName.substring(0,(info.versionName.length() - 2));
	}
	
	public static String getAppVersionName(Context context) {
		
		PackageInfo info = Util.getAppVersionInfo(context);
		return info.versionName;
	}
	
	public static int getAppVersionCode(Context context) {
		
		PackageInfo info = Util.getAppVersionInfo(context);
		return info.versionCode;
	}
	
	@SuppressLint("MissingPermission")
	public static String getImei(Context context) {
		
		TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return mngr.getDeviceId();
	}

	public static boolean isInternetConnected(Context context) {

		boolean retorno = true;
		ConnectivityManager mngr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if(mngr != null) {
			NetworkInfo netInfo = mngr.getActiveNetworkInfo();
	        if (netInfo == null) {
	        	retorno = false;
	        } else {
		        int netType = netInfo.getType();
		        if(netType == ConnectivityManager.TYPE_WIFI || netType == ConnectivityManager.TYPE_MOBILE) {
		        	retorno = netInfo.isConnected();
		        } else {
		        	retorno = false;
		        }
	        }
		} else {
			retorno = false;
		}
		return retorno;
	}
	
	public static boolean isServiceRunning(Context context, String servico) {
		
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	    	if (servico.equals(service.service.getClassName())) {
	                return true;
	        }
	    }
	    
	    return false;
	}	
	
	public static String stringJoin(String separator, String[] array) {
	
		StringBuilder sbStr = new StringBuilder();
	    for (int i = 0, il = array.length; i < il; i++) {
	        if (i > 0)
	            sbStr.append(separator);
	        sbStr.append(array[i]);
	    }
	    return sbStr.toString();
	}

	public static Date dataHoraAtual() {
		
		Date data = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.getTime();
	}

	public static Date dataParser(String data, String formatoEntrada) {
		
		Date retorno = null;
		SimpleDateFormat formatter = new SimpleDateFormat(formatoEntrada);
		try {
			retorno = formatter.parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
	public static String dataFormatar(Date data, String formatoEntrada) {
		
		SimpleDateFormat formatter = new SimpleDateFormat(formatoEntrada);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        return formatter.format(calendar.getTime());
	}
	
	public static String dataFormatar(String data, String formatoEntrada, String formatoSaida) {
		
		SimpleDateFormat formatterOut = new SimpleDateFormat(formatoSaida);
		return formatterOut.format(dataParser(data, formatoEntrada));
	}
	
	public static String moedaFormatar(String numero) {

		double numero_double = Double.parseDouble(numero);
		if(Util.dfs == null) {
			dfs = new DecimalFormatSymbols(Locale.getDefault());
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			formatterNumber = new DecimalFormat("#,##0.00", dfs);
		}
		return formatterNumber.format(numero_double);
	}
	
	public static String pathFile (Context context, Uri selectedFile, String[] filePathColumn) {
		
	    Cursor cursor = context.getContentResolver().query(selectedFile, filePathColumn, null, null, null);
	    cursor.moveToFirst();
	    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	    String filePath = cursor.getString(columnIndex);
	    cursor.close();
	    
	    return filePath;
	}
	
	public static String dac_10(String valorBase, int numDigitos) {

		int somatorio = 0;
		int fatorMultiplicador = 2;
		String valorTemporario = "";
		
		for(int n = 0 ; n < numDigitos ; n++) {

			somatorio = 0;
			valorTemporario = "";
			fatorMultiplicador = 2;
			
			for(int i = (valorBase.length()-1) ; i >= 0 ; i--) {
				valorTemporario = (fatorMultiplicador * Integer.parseInt(valorBase.subSequence(i, i+1).toString())) + valorTemporario;
				if(--fatorMultiplicador < 1) {
					fatorMultiplicador = 2;
				}
			}
			
			for(int i = 0 ; i < valorTemporario.length() ; i++) {
				somatorio = somatorio + Integer.parseInt(valorTemporario.subSequence(i, i+1).toString());
			}

			somatorio = somatorio % 10;
			
			if(somatorio != 0) {
				somatorio = 10 - somatorio;
			}
			
			valorBase += somatorio+"";
		}
		
		return String.format("%1$0"+numDigitos+"d", Integer.parseInt(valorBase.subSequence(valorBase.length()-numDigitos, valorBase.length()).toString()));
	}
	
	public static String dac_11(String valorBase, int numDigitos, int limiteMultiplicador) {
		
		int temp = 0;
		int somatorio = 0;
		int fatorMultiplicador = 0;
		for(int n = 0 ; n < numDigitos ; n++) {
			fatorMultiplicador = limiteMultiplicador;
			somatorio = 0;
			for(int i = (valorBase.length()-1) ; i >= 0 ; i--) {
				if(isNumeric(valorBase.subSequence(i, i+1).toString())) {
					temp = Integer.parseInt(valorBase.subSequence(i, i+1).toString());
					somatorio += temp * fatorMultiplicador;
					if(fatorMultiplicador == 0) {
						fatorMultiplicador = limiteMultiplicador;
					} else {
						fatorMultiplicador--;
					}
				}
			}
			valorBase += Integer.toString(((somatorio%11)%10));
		}
		
		return String.format("%1$0"+numDigitos+"d", Integer.parseInt(valorBase.subSequence(valorBase.length()-numDigitos, valorBase.length()).toString()));
	}
	
	public static boolean isNumeric(String str) {
		
	    try {
	    	Double.parseDouble(str);
	    } catch(NumberFormatException nfe) {
	    	return false;
	    }
	    
	    return true;
	}	
}
