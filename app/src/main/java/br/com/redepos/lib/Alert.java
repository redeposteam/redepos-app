package br.com.redepos.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class Alert {

	public static void show(Activity activity, String titulo, String mensagem) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(titulo).setMessage(mensagem);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			} 
		});
		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.show();
	}
	
	public static void show(Activity activity, String titulo, String mensagem, OnClickListener callback) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(titulo).setMessage(mensagem);
		builder.setPositiveButton("OK", callback);
		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.show();
	}
}

