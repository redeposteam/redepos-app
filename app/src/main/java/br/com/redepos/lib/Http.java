package br.com.redepos.lib;

import java.io.IOException;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.util.Set;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

public class Http {

	protected URL url;
	protected HttpURLConnection connection;
	protected HashMap<String, String> parameters;

	public Http() {

		parameters = new HashMap<String, String>();
	}

	public void addParameter(String key, String value) {

		parameters.put(key, value);
	}

	public void removeParameter(String key) {

		parameters.remove(key);
	}
	
	public void removeParameterArray(String[] key) {
		
		for (int i = 0; i < key.length; i++) {
			
			removeParameter(key[i]);
		}
	}

	public void setParameter(String key, String value) {

		parameters.put(key, value);
	}

	public String getParameters() {

		String[] params = new String[parameters.size()];
		Set<Entry<String, String>> set = parameters.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();

		int i = 0;
		String key;
		String val;

		while (iterator.hasNext()) {
			Entry<String, String> m = iterator.next();
			key = (String) m.getKey();
			val = (String) m.getValue();
			params[i++] = key + "=" + val;
		}

		if (params.length > 0) {
			return Util.stringJoin("&", params);
		} else {
			return "";
		}
	}

	public String send(String host) throws MalformedURLException, TimeoutException, IOException {

		String response = null;

		// Propriedades da Conexao
		
		url = new URL(host);
		connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(Constants.SERVICE_TIMEOUT);

		// Dados da Requisicao

		byte[] dados = this.getParameters().getBytes("UTF-8");

		// Propriedades da Requisicao

		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", String.valueOf(dados.length));
		connection.setDoOutput(true);

		// Envio da Requisicao

		this.connection.getOutputStream().write(dados);

		// Resposta do Servidor

		String resposta = "";
		Scanner scanner = new Scanner(connection.getInputStream());
		while (scanner.hasNextLine()) {
			resposta += (scanner.nextLine());
		}
		if(resposta.length() > 0){
			response = resposta;
		}
		scanner.close();

		// Fechamento da Conexao

		connection.disconnect();

		return response;
	}
}
