package br.com.redepos.lib;

import android.net.Uri;
import android.content.Intent;
import android.media.Ringtone;
import android.content.Context;
import android.app.Notification;
import android.app.PendingIntent;
import android.media.RingtoneManager;
import android.graphics.BitmapFactory;
import android.app.NotificationManager;

public class Notifify {

	private static int[] PRIORIDADES = new int[]{Notification.PRIORITY_MAX, Notification.PRIORITY_HIGH, Notification.PRIORITY_DEFAULT, Notification.PRIORITY_LOW, Notification.PRIORITY_MIN}; 
	
	public static String PRIORIDADE_MAXIMA = "0"; 
	public static String PRIORIDADE_ALTA   = "1"; 
	public static String PRIORIDADE_PADRAO = "2"; 
	public static String PRIORIDADE_BAIXA  = "3"; 
	public static String PRIORIDADE_MINIMA = "4";
	
	public static void show(Context context, Class<?> pendingActivity, String textoRapido, String titulo, String mensagem, int iconePequeno, int iconeGrande, long[] vibracao, String prioridade) {

		NotificationManager nmngr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		Notification.Builder builder = new Notification.Builder(context);
		
		int priority;
		if(prioridade == null) {
			priority = Notifify.PRIORIDADES[Integer.parseInt(Notifify.PRIORIDADE_PADRAO)];
		} else {
			priority = Notifify.PRIORIDADES[Integer.parseInt(prioridade)];
		}

		if(pendingActivity !=null) {
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, pendingActivity), 0);
			builder.setContentIntent(pendingIntent);
		}
		
		builder.setPriority(priority);
		builder.setTicker(textoRapido);
		builder.setContentTitle(titulo);
		builder.setContentText(mensagem);
		builder.setSmallIcon(iconePequeno);
		builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), iconeGrande));
		
		Notification notification = builder.build();
		
		notification.flags = Notification.FLAG_AUTO_CANCEL;

		if(vibracao != null) {
			notification.vibrate = vibracao;
		}
		
		nmngr.notify(iconePequeno, notification);

		try{
			Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone ring = RingtoneManager.getRingtone(context, sound);
			ring.play();
		} catch(Exception e) {
			
		}
	}
}