package br.com.redepos.lib;

public class Constants {

	// SHERADPREFERENCES

	public static final String CONFIG_FILE = "REDEPOS_PREFERENCES";

	// DATABASE

	public static final String DB_NAME = "REPOSMOBILE";
	public static final int DB_VERSION = 6;

	public static final int SERVICE_TIMEOUT = 3000;

	public static final String TRANSACAO_LOGIN = "1";
	public static final String TRANSACAO_ENCERRAMENTO = "2";
	//public static final String TRANSACAO_ENCERRAMENTO = "9";

	public static final String TRANSACAO_ATUALIZACAO = "3";
	public static final String TRANSACAO_SOLICITACAO = "4";
	public static final String TRANSACAO_ARRECADACAO = "5";
	public static final String TRANSACAO_QRCODE = "2";

	// CONNECTION

	public static final String CONNECTION_ERRO = "connectionerro";
	public static final String CONNECTION_TIMEOUT = "connectiontimeout";
	public static final String CONNECTION_ERRO_URL = "connectionerrourl";

	// DOWNLOAD

	public static final String DOWNLOAD_APK_FILE = "redeposmobile.apk";
	public static final String DOWNLOAD_ERRO = "downloadtimeout";
	public static final String DOWNLOAD_ERRO_ARQUIVO = "downloaderroarquivo";
	public static final String DOWNLOAD_ERRO_PROTOCOLO = "downloaderroprotocolo";

	// STATUS

	public static final int STATUS_OK = 1;
	public static final int STATUS_ERRO = 2;

	// EVENTOS

	public static final String EVENTO_ENCERRAMENTO = "Envio de Encerramento";
	public static final String EVENTO_SOLICITACAO_LOTE = "Solicitao de Lote";
	public static final String EVENTO_ARRECADACAO = "Envio de Arrecadao";
	public static final String EVENTO_QRCODE = "Envio de Qrcode";

	// ENCERRAMENTO

	public static final int ENCERRAMENTO_PENDENTE = 0;
	public static final int ENCERRAMENTO_ENVIADO = 1;
	public static final int ENCERRAMENTO_ENVIADO_ERRO = 2;

	// ARRECADACAO

	public static final int ARRECADACAO_PENDENTE = 0;
	public static final int ARRECADACAO_ENVIADO = 1;
	public static final int ARRECADACAO_ENVIADO_ERRO = 2;

	// QRCODE

	public static final int ENVIOQRCODE_PENDENTE = 0;
	public static final int ENVIOQRCODE_ENVIADO = 1;
	public static final int ENVIOQRCODE_ENVIADO_ERRO = 2;
}
