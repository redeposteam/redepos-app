package br.com.redepos.lib;

import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {

	public static SharedPreferences getSharedPreferences(Activity activity) {

		return activity.getSharedPreferences(Constants.CONFIG_FILE, Activity.MODE_PRIVATE);
	}

	public static void setStrings(Activity activity, HashMap<String, String> params) {
		
		SharedPreferences preferences = getSharedPreferences(activity);
		
		Set<String> set = params.keySet();
		Iterator<String> iterator = set.iterator();
		Editor editor = preferences.edit();
		
		String k = null;
		while (iterator.hasNext()) {
			k = (String) iterator.next();
			editor.putString(k, params.get(k));
		}
		editor.commit();
	}
	
	public static void setIntegers(Activity activity, HashMap<String, Integer> params) {
		
		SharedPreferences preferences = getSharedPreferences(activity);
		
		Set<String> set = params.keySet();
		Iterator<String> iterator = set.iterator();
		Editor editor = preferences.edit();
		
		String k = null;
		while (iterator.hasNext()) {
			k = (String) iterator.next();
			editor.putInt(k, params.get(k));
		}
		editor.commit();
	}
}