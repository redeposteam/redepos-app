package br.com.redepos.lib;

import android.graphics.Paint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.Bitmap.Config;
import android.graphics.ColorMatrixColorFilter;

public class Graphic {

	public static int[] calcResize (Bitmap bmpOriginal, int width) {
		
		int NA = 0; 
		int NL = width;
        int VL = bmpOriginal.getWidth();
        int VA = bmpOriginal.getHeight();
        NA = (int) Math.floor(((double)VA/(double)VL)*(double) NL);
        
        return new int[]{NL, NA};
	}
	
	public static Bitmap toGrayscale (Bitmap bmpOriginal, Config config) {

		int width  = bmpOriginal.getWidth();    
        int height = bmpOriginal.getHeight();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, config);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        
        return bmpGrayscale;
	}
}