package br.com.redepos;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;

import com.google.zxing.Result;
import com.journeyapps.barcodescanner.CaptureActivity;

public class ZxingActivity extends CaptureActivity {

	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
	
		Intent itent = getIntent();
		itent.putExtra("SCAN_RESULT", rawResult.getText());
		itent.putExtra("SCAN_FORMAT", rawResult.getBarcodeFormat().toString());
		setResult(Activity.RESULT_OK, itent);
		finish();
	}
}