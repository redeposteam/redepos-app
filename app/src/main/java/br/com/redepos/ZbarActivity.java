package br.com.redepos;

import android.app.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.widget.FrameLayout;
import android.widget.TextView;

import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;

import com.yanzhenjie.zbar.Config;
import com.yanzhenjie.zbar.Image;
import com.yanzhenjie.zbar.ImageScanner;
import com.yanzhenjie.zbar.Symbol;
import com.yanzhenjie.zbar.SymbolSet;

/*********** 
 IMPORT ZBAR
 ***********/

//import net.sourceforge.zbar.ImageScanner;
//import net.sourceforge.zbar.Image;
//import net.sourceforge.zbar.Symbol;
//import net.sourceforge.zbar.SymbolSet;
//import net.sourceforge.zbar.Config;

public class ZbarActivity extends AbstractActivity {

	ImageScanner scanner;
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;

	private boolean previewing = true;
	static {System.loadLibrary("iconv");}

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		definirEstilos();

		setContentView(R.layout.zbar);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		mCamera = getCameraInstance();
		autoFocusHandler = new Handler();

		/****************************
		 INSTNCIA DO BARCODE SCANNER
		 ****************************/

		scanner = new ImageScanner();
		scanner.setConfig(0, Config.ENABLE, 0);
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);
		scanner.setConfig(Symbol.QRCODE, Config.ENABLE, 1);

		/***************************************
		 INSTNCIA DE PRE-VISUALIZAO DA CMERA
		 ***************************************/

		TextView statusView = (TextView) findViewById(R.id.label_captura);
		statusView.setText(getIntent().getExtras().getString("labelCaptura"));
		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB, getIntent().getExtras().getInt("cor"));
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);
	}

	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	public static Camera getCameraInstance() {

		Camera camera = null;
		try {
			camera = Camera.open();
		} catch (Exception e) {
		}
		return camera;
	}

	private void releaseCamera() {

		if (mCamera != null) {
			previewing = true;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {

		public void run() {
			if (previewing && mCamera != null) {
				mCamera.autoFocus(autoFocusCB);
			}
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {

		public void onPreviewFrame(byte[] data, Camera camera) {

			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {

				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				String dados = "";
				SymbolSet syms = scanner.getResults();

				for (Symbol sym : syms) {
					dados+= sym.getData();
				}

				Intent intent = new Intent();
				intent.putExtra("SCAN_RESULT", dados);
				ZbarActivity.this.setResult(Activity.RESULT_OK, intent);
				ZbarActivity.this.finish();
			}
		}
	};

	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {

		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 300);
		}
	};
}
