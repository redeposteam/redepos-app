package br.com.redepos;

import android.os.Bundle;
import android.app.AlertDialog;
import br.com.redepos.menu.Menu;
import br.com.redepos.lib.Preferences;
import br.com.redepos.services.UpdateService;

import android.content.Intent;
import android.content.DialogInterface;
import android.content.SharedPreferences;

public class MainActivity extends AbstractActivity {

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		definirEstilos();

		//MENU PRINCIPAL
		new Menu(this);
	}

	@Override
	protected void onResume() {

		// VERIFICAR VERSAO
		checarVersao();
		super.onResume();
	}

	@Override
	public void onBackPressed() {

		sair();
	}

	protected void checarVersao() {

		SharedPreferences preferences = Preferences.getSharedPreferences(this);

		if(!preferences.getString("versao_autorizada", "").equals("true")) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getApplicationContext().getString(R.string.dialog_title_update_app)).setMessage(getApplicationContext().getString(R.string.app_message_versao_nao_autorizada));
			builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					//startService(new Intent("UPDATE_SERVICE"));
					UpdateService.enqueueWork(MainActivity.this, new Intent());
					finish();
				}
			});
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					finish();
				}
			});
			AlertDialog alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}
	}
}
