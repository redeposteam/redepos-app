package br.com.redepos;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import br.com.redepos.lib.Util;
import br.com.redepos.lib.Preferences;
import br.com.redepos.tasks.LoginTask;

public class LoginActivity extends Activity {

	private SharedPreferences preferences;
	private EditText usuario;
	private EditText senha;
	private Spinner tipo;
	private Button enviar;
	private Button limpar;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(br.com.redepos.R.layout.activity_login);

		this.setVersionProperties();
		
		// PREFERENCIAS
		
		preferences = Preferences.getSharedPreferences(this);
		
		// CONFIGURACOES
		
		String sUsuarioLogin = preferences.getString("usuarioLogin", "");
		int sUsuarioPerfil = Integer.parseInt(preferences.getString("usuarioPerfil", "0")) - 1;
		
		// RECUPERANDO OBJETOS
		
		usuario = (EditText) findViewById(R.id.login_campo_usuario);
		senha = (EditText) findViewById(R.id.login_campo_senha);
		tipo = (Spinner) findViewById(br.com.redepos.R.id.tipoAcesso);
		enviar = (Button) findViewById(R.id.login_botao_enviar);
		limpar = (Button) findViewById(R.id.login_botao_limpar);
		
		// DADOS DE INTERFACE

		usuario.setText(sUsuarioLogin);
		
		tipo.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,new String[]{
												 this.getString(R.string.login_combo_regional),
												 this.getString(R.string.login_combo_arrecadador),
												 this.getString(R.string.login_combo_vendedor),
												 this.getString(R.string.login_combo_empresa)}
												));
		tipo.setSelection(sUsuarioPerfil);
		
		// EVENTOS DE INTERFACE

		usuario.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					senha.requestFocus();
					return true;
				}
				return false;
			}
		});
		
		senha.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					enviar.callOnClick();
					return true;
				}
				return false;
			}
		});		
		
		limpar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				senha.setText(null);
				usuario.setText(null);
			}
		});
		
		enviar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				new LoginTask(LoginActivity.this).execute
				(
					Integer.toString(tipo.getSelectedItemPosition()+1),
					usuario.getText().toString().trim(), 
					senha.getText().toString().trim()
				);
			}
		});
	}

	private void setVersionProperties() {
        ((TextView) findViewById(R.id.versao)).setText(Util.getAppVersionName(this));
        TextView buildType = findViewById(R.id.buildType);
        TextView flavor = findViewById(R.id.flavor);
		if (BuildConfig.DEBUG_MODE) {
            buildType.setVisibility(View.VISIBLE);
            flavor.setVisibility(View.VISIBLE);
            buildType.setText(String.format("Build: %s", BuildConfig.BUILD_TYPE));
			flavor.setText(String.format("Flavor: %s", BuildConfig.FLAVOR));
		} else {
            buildType.setVisibility(View.GONE);
            flavor.setVisibility(View.GONE);
        }
	}

	protected void onResume() {
		
		super.onResume();
		
		if(!Util.isServiceRunning(this, "br.com.redepos.services.UpdateService")) {

			if(usuario.getText().toString().length() > 0) {
				senha.requestFocus();
			}
			
		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getApplicationContext().getString(R.string.dialog_title_update_app)).setMessage(getApplicationContext().getString(R.string.app_message_download));
			builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() { 
				public void onClick(DialogInterface dialog, int id) {
					finish();
				} 
			});
			AlertDialog alert = builder.create();
			alert.setCancelable(false);			
			alert.show();
		}
	}
}
