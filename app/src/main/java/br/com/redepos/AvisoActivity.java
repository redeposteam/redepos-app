package br.com.redepos;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AvisoActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aviso);
		definirEstilos();
		Bundle bundle = getIntent().getExtras();
	    ((TextView) findViewById(R.id.aviso_mensagem)).setText(bundle.getString("mensagem"));
	    Button ok = (Button) findViewById(R.id.aviso_botao_ok);
	    
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				finish();
			}
		});
	}
}
