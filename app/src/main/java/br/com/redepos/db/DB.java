package br.com.redepos.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DB {

	private SQLiteDatabase db;

	public DB (Context context) {
		
		DBcore aux = new DBcore(context);
		db = aux.getWritableDatabase(); 
	}
	
	public SQLiteDatabase getSQLite() {
	
		return db;
	}

	public long inserir(String table, ContentValues valores) {
	
		return db.insert(table, null, valores);
	}
	
	public void atualizar(String table, String primaryKey, ContentValues valores) {
		
		db.update(table, valores, primaryKey + " = ?", new String[]{valores.getAsString(primaryKey)});
	}

	public void atualizarStatusAuditoria(long id, String coluna, ContentValues dados) {
		db.update("auditoria", dados,  coluna + " = ?", new String[]{ String.valueOf(id) });
	}
	
	public void deletar(String table, String primaryKey, String id) {

		db.delete(table, primaryKey + " = ?", new String[]{id});
	}

	public Cursor selecionar(String table, String[] columns, String where, String[] whereArgs, String groupBy, String orderBy, String limit) {

		Cursor cursor = db.query(table, columns, where, whereArgs, groupBy, null, orderBy, limit);
		return cursor;
	}

}
