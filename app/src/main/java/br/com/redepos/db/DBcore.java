package br.com.redepos.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.com.redepos.lib.Constants;

public class DBcore extends SQLiteOpenHelper {

	public DBcore(Context context) {

		super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
	}

	//QUANDO NO EXISTE BANCO

	public void onCreate(SQLiteDatabase db) {

		db.execSQL("CREATE TABLE usuario " +
				"(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"perfil TEXT NOT NULL, " +
				"login TEXT NOT NULL, " +
				"senha TEXT NOT NULL, " +
				"dados TEXT NOT NULL);");
		db.execSQL("CREATE TABLE auditoria " +
				"(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"id_tipo_loteria INTEGER, " +
				"loteria TEXT, " +
				"codigo TEXT, " +
				"status INTEGER, " +
				"evento TEXT NOT NULL, " +
				"usuario TEXT NOT NULL, " +
				"data DATETIME NOT NULL, " +
				"data_servidor DATETIME, " +
				"id_encerramento INTEGER, " +
				"id_arrecadacao INTEGER, " +
				"transacao TEXT);");
		db.execSQL("CREATE TABLE encerramento " +
				"(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"perfil TEXT NOT NULL, " +
				"login TEXT NOT NULL, " +
				"senha TEXT NOT NULL, " +
				"id_tipo_loteria INTEGER, " +
				"loteria TEXT, " +
				"codigo TEXT, " +
				"enviado INTEGER, " +
				"data_criacao DATETIME NOT NULL, " +
				"data_envio DATETIME NOT NULL, " +
				"transacao TEXT);");
		db.execSQL("CREATE TABLE arrecadacao " +
				"(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"perfil TEXT NOT NULL, " +
				"login TEXT NOT NULL, " +
				"senha TEXT NOT NULL, " +
				"id_tipo_loteria INTEGER, " +
				"loteria TEXT, " +
				"codigo TEXT, " +
				"enviado INTEGER, " +
				"data_criacao DATETIME NOT NULL, " +
				"data_envio DATETIME NOT NULL, " +
				"transacao TEXT);");
	}

	//QUANDO J EXISTE BANCO

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		Log.i("teste", "OLD_VERSION: " + String.valueOf(oldVersion));
		Log.i("teste", "NEW_VERSION: " + String.valueOf(newVersion));

		switch (oldVersion) {
			case 1:
				//VERSO 1 => 2
				db.execSQL("CREATE TABLE usuario (id INTEGER PRIMARY KEY AUTOINCREMENT, perfil TEXT NOT NULL, login TEXT NOT NULL, senha TEXT NOT NULL, dados TEXT NOT NULL);");
				db.execSQL("CREATE TABLE encerramento (id INTEGER PRIMARY KEY AUTOINCREMENT, perfil TEXT NOT NULL, login TEXT NOT NULL, senha TEXT NOT NULL, id_tipo_loteria INTEGER, loteria TEXT, codigo TEXT, enviado INTEGER, data_criacao DATETIME NOT NULL, data_envio DATETIME NOT NULL);");
				break;
			case 2:
				//VERSO 2 => 3
				db.execSQL("ALTER TABLE auditoria ADD transacao TEXT;");
				db.execSQL("ALTER TABLE encerramento ADD transacao TEXT;");
				break;
			case 3:
				//VERSO 3 => 4
				db.execSQL("CREATE TABLE arrecadacao (id INTEGER PRIMARY KEY AUTOINCREMENT, perfil TEXT NOT NULL, login TEXT NOT NULL, senha TEXT NOT NULL, id_tipo_loteria INTEGER, loteria TEXT, codigo TEXT, enviado INTEGER, data_criacao DATETIME NOT NULL, data_envio DATETIME NOT NULL, transacao TEXT);");
				break;
			case 4:
				//VERSO 4 => 5
				break;
			case 5:
				//VERSO 5 => 6
				db.execSQL("ALTER TABLE auditoria ADD data_servidor DATETIME;");
				db.execSQL("ALTER TABLE auditoria ADD id_encerramento INTEGER;");
				db.execSQL("ALTER TABLE auditoria ADD id_arre6cadacao INTEGER;");
				break;
			default:
				throw new IllegalStateException("onUpgrade() com verso antiga desconhecida " + oldVersion);
		}
	}
}
