package br.com.redepos;

import java.io.IOException;

import br.com.redepos.lib.Util;
import android.content.Context;

import android.util.Log;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.AutoFocusCallback;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    public static final int BORDER_RED   = 1;
    public static final int BORDER_BLUE  = 2;
    public static final int BORDER_GREEN = 3;

    private Camera mCamera;
    private SurfaceHolder mHolder;
    private PreviewCallback previewCallback;
    private AutoFocusCallback autoFocusCallback;

    @SuppressWarnings("deprecation")
    public CameraPreview(Context context, Camera camera, PreviewCallback previewCb, AutoFocusCallback autoFocusCb, int borderColor) {

        super(context);
        mCamera = camera;
        previewCallback = previewCb;
        autoFocusCallback = autoFocusCb;

        switch (borderColor) {

            case CameraPreview.BORDER_RED:
                this.setBackground(getResources().getDrawable(R.drawable.border_red));
                break;
            case CameraPreview.BORDER_BLUE:
                this.setBackground(getResources().getDrawable(R.drawable.border_blue));
                break;
            case CameraPreview.BORDER_GREEN:
                this.setBackground(getResources().getDrawable(R.drawable.border_green));
                break;
        }
        /*
         * Ajusta a camera para foco continuo se for suportado, caso contrario
         * ser usuado o foco automtico. Funciona apaenas para API >= 9
         */

        Camera.Parameters parameters = camera.getParameters();
        for (String f : parameters.getSupportedFocusModes()) {
            if (f == Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
                parameters.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                autoFocusCallback = null;
                break;
            }
        }

        /*
         * Configurando um SurfaceHolder.Callback para ser notificado quando a
         * superfcie subjacente  criada e destruda
         */

        mHolder = getHolder();
        mHolder.addCallback(this);

        /*
         * definio obsoleta, mas  necessria em
         * verses anteriores ao Android 3.0
         */
        /*
        if(Util.getAPIVerison() < 3.0) {
        	mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        */
    }

    public void surfaceCreated(SurfaceHolder holder) {

        /*
         * A superfcie foi criada, agora  s dizer a cmera onde desenhar
         * a pr-visualizao.
         */

        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            Log.d("DBG", "Erro na Configurao de Pre-Visualizao da Cmera: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

        // Camera preview released in activity
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        /*
         * Se a sua pr-visualizao mudar ou girar, cuidar desses eventos aqui.
         * Certifique-se de parar a pr-visualizao antes de redimensionar ou reformat-la.
         */

        //Superfcie de visualizao no existe
        if (mHolder.getSurface() == null) {
            return;
        }

        //Parar a pr-visualizao antes de fazer alteraes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
        }

        try {
            //Rotao superfcie cmera fixo rgido de 90 degs para coincidir com visualizao em retrato
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(previewCallback);
            mCamera.startPreview();
            mCamera.autoFocus(autoFocusCallback);
        } catch (Exception e) {
            Log.d("DBG", "Erro na Configurao de Pre-Visualizao da Cmera: " + e.getMessage());
        }
    }
}
