package br.com.redepos;

import br.com.redepos.services.NotificacaoService;
import br.com.redepos.LoginActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class SplashActivity extends Activity implements Runnable {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		checkPermitions();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		checkPermitions();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStop() {
//		Intent notificationIntent = new Intent(this, NotificacaoService.class);
//		this.startService(notificationIntent);
		NotificacaoService.enqueueWork(this, new Intent());
//		NotificacaoService.enqueueWork(this, new Intent());
		super.onStop();
	}

	@Override
	public void run() {

		//Implementacao
		startActivity(new Intent(SplashActivity.this, LoginActivity.class));
		finish();
	}

	private void alertDeniedPermissions() {
		AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
		builder.setTitle(getString(R.string.permissoes))
				.setMessage(getString(R.string.permissoes_necessarias));
		builder.setPositiveButton(getString(R.string.configuracoes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
						Uri.fromParts("package", getPackageName(), null));
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		builder.setNegativeButton(getString(R.string.sair_do_app), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	private void checkPermitions() {
		Dexter.withActivity(this)

				.withPermissions(
						Manifest.permission.CAMERA,
						Manifest.permission.READ_PHONE_STATE,
						Manifest.permission.ACCESS_NETWORK_STATE,
						Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.WAKE_LOCK)
				.withListener(new MultiplePermissionsListener() {
					@Override
					public void onPermissionsChecked(MultiplePermissionsReport report) {
						// check if all permissions are granted
						if (report.areAllPermissionsGranted()) {
							Handler SplashScreen = new Handler();
							SplashScreen.postDelayed(SplashActivity.this, 1000);
						} else {
							SplashActivity.this.alertDeniedPermissions();
						}
					}

					@Override
					public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
						token.continuePermissionRequest();
					}
				})
				.onSameThread()
				.check();
	}
}
